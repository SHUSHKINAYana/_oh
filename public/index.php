<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
if(!empty($_COOKIE['changeAdmin'])&&!empty($_COOKIE['loginAdmin'])){
    $id= $_GET['id'];
    $login = $_GET['login'];
    $_SESSION['login'] = $login;
    $fio = $_GET['fio'];
    $email = $_GET['email'];
    $yob = $_GET['yob'];
    $n_limbs = $_GET['n_limbs'];
    $gender = $_GET['gender'];
    $bio = $_GET['bio'];
    $spspn = $_GET['spsp'];
    $_SESSION['uid'] = $id;
    setcookie('fio_value', $fio, time() + 30 * 24 * 60 * 60);
    setcookie('email_value', $email, time() + 30 * 24 * 60 * 60);
    setcookie('yob_value', $yob, time() + 30 * 24 * 60 * 60);
    setcookie('n_limbs_value', $n_limbs, time() + 30 * 24 * 60 * 60);
    setcookie('gender_value', $gender, time() + 30 * 24 * 60 * 60);
    setcookie('bio_value', $bio, time() + 30 * 24 * 60 * 60);
    setcookie('login', $login, time() + 30 * 24 * 60 * 60);
    setcookie('sp-sp_value', $spspn, time() + 30 * 24 * 60 * 60);
    setcookie('pass', '', -(time() + 30 * 24 * 60 * 60));
    setcookie('changeAdmin', '', -(time() + 30 * 24 * 60 * 60));
    header('Location: ./');
    
}



if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    if (!empty($_COOKIE['save'])) {
        
        setcookie('save', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass'])) {
          $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
            и паролем <strong>%s</strong> для изменения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
        }

    }

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['yob'] = !empty($_COOKIE['yob_error']);
    $errors['n_limbs'] = !empty($_COOKIE['n_limbs_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['sp-sp'] = !empty($_COOKIE['sp-sp_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['Galochka'] = !empty($_COOKIE['Galochka_error']);
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="fioerror">  Заполните имя.</div>';

    }
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="emailerror">  Заполните почту.</div>';
    }
    if ($errors['yob']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('yob_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="yoberror">  Заполните дату.</div>';
    }
    if ($errors['n_limbs']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('n_limbs_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="n_limbserror">  Выберите количество конечностей.</div>';
    }
    if ($errors['gender']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('gender_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="gendererror">  Заполните пол.</div>';
    }
    if ($errors['sp-sp']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('sp-sp_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="sp-sperror">  Выберите сверхспособность.</div>';
    }
    if ($errors['bio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('bio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="bioerror">  Заполните биографию.</div>';
    }
    if ($errors['Galochka']) {
        $messages[] = '<div class="Galochkaerror">  Поставьте галочку.</div>';
    }
    if (!$errors['fio'] && !$errors['email'] && !$errors['yob'] && !$errors['n_limbs'] && !$errors['gender'] && !$errors['sp-sp'] && !$errors['bio'] && !$errors['Galochka']  ) {
        if (!empty($_COOKIE['save'])) {
            // Удаляем куку, указывая время устаревания в прошлом.
            setcookie('save', '', 100000);
            // Если есть параметр save, то выводим сообщение пользователю.
        
        }
    }
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['yob'] = empty($_COOKIE['yob_value']) ? '' : $_COOKIE['yob_value'];
    $values['n_limbs'] = empty($_COOKIE['n_limbs_value']) ? '' : $_COOKIE['n_limbs_value'];
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['sp-sp'] = empty($_COOKIE['sp-sp_value']) ? '' : $_COOKIE['sp-sp_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['Galochka'] = empty($_COOKIE['Galochka_value']) ? '' : $_COOKIE['Galochka_value'];
    if (isset($_SESSION['login'])) {
        $login = $_SESSION['login'];
        $conn = new PDO("mysql:host=localhost;dbname=u24052", 'u24052', '4700864', array(PDO::ATTR_PERSISTENT => true));
        $user = $conn->prepare("UPDATE users SET fio = ?, email = ?, yob = ?, gender = ?, n_limbs = ?, bio = ? WHERE `login`='$login'");
        if(isset($_COOKIE['fio_value'])){
            $user -> execute([$_COOKIE['fio_value'], $_COOKIE['email_value'], $_COOKIE['yob_value'], $_COOKIE['gender_value'], $_COOKIE['n_limbs_value'], $_COOKIE['bio_value']]);
            setcookie('save', '', 100000);
        }
        $pathtoabilil = $conn-> query("SELECT * FROM abilitys WHERE `id_user` = '$_SESSION[uid]' ");
        $idabil = $pathtoabilil->fetch(PDO::FETCH_ASSOC);
        $abfafa = $conn->prepare("UPDATE `ability` SET names_ability = ? WHERE `abilitysId`= $idabil[id_abilitys]");
        $abfafa->execute([$_COOKIE['sp-sp_value']]);
        $messages[] = '<br>Выполнен вход с логином  ' .$_SESSION['login'];
    }

    include('form.php');
    
}
else{
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }else{
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['yob'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('yob_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('yob_value', $_POST['yob'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['n_limbs'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('n_limbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('n_limbs_value', $_POST['n_limbs'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['gender'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['bio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['sp-sp'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('sp-sp_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('sp-sp_value', $_POST['sp-sp'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['Galochka'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('Galochka_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('Galochka_value', $_POST['Galochka'], time() + 30 * 24 * 60 * 60);
    }
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
      }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('yob_error', '', 100000);
        setcookie('n_limbs_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('sp-sp_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('Galochka_error', '', 100000);
    }
    
    if (!empty($_SESSION['login'])){
        $conn = new PDO("mysql:host=localhost;dbname=u24052", 'u24052', '4700864', array(PDO::ATTR_PERSISTENT => true));
        $user = $conn->prepare("UPDATE users SET fio = ?, email = ?, yob = ?, gender = ?, n_limbs = ?, bio = ?, login = ?, pass = ? WHERE `login`='$login' AND `pass` = 
        '$pass'");
        if(isset($_POST['fio'])){
            $user -> execute([$_POST['fio'], $_POST['email'], $_POST['yob'], $_POST['gender'], $_POST['n_limbs'], $_POST['bio'], $login, $passmd5 ]);
            setcookie('save', '', 100000);
            
        }
        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    }
    else {
        //создаем логин и пароль для пользователя
        $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $maxp=10;
        $maxl=5;
        $size=StrLen($chars)-1;
        $pass=null;
        while($maxp--){
            $pass.=$chars[rand(0,$size)];
        }
        while($maxl--){
            $login.=$chars[rand(0,$size)];
        }
        $passmd5 = md5($pass);
        setcookie('login', $login);
        setcookie('pass', $pass);
        
        //вставляем нового пользователя
        $conn = new PDO("mysql:host=localhost;dbname=u24052", 'u24052', '4700864', array(PDO::ATTR_PERSISTENT => true));
        $user = $conn->prepare("INSERT INTO users SET fio = ?, email = ?, yob = ?, gender = ?, n_limbs = ?, bio = ?, login = ?, pass = ?");
        if(isset($_POST['fio'])){
            $user -> execute([$_POST['fio'], $_POST['email'], $_POST['yob'], $_POST['gender'], $_POST['n_limbs'], $_POST['bio'], $login, $passmd5 ]);
            setcookie('save', '', 100000);
            
        }
        $id_user = $conn->lastInsertId();
        $abilitys = $conn->prepare("INSERT INTO abilitys SET id_user = ?");
        $abilitys -> execute([$id_user]);
        $id_abil = $conn->lastInsertId();
        if(isset($_POST['sp-sp'])){
            foreach($_POST['sp-sp'] as &$abil){
                $ability = $conn->prepare("INSERT INTO ability SET abilitysId = ?, names_ability = ?");
                $ability -> execute([$id_abil, $abil]);
            }
            
        }
        
    }

    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');

    // Делаем перенаправление.
    header('Location: ./');

   
}
?>
