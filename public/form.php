<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="style.css">
  <meta charset="utf-8">
  <title>.pixel</title>
</head>
<body >
  <header>
  <div class="banner">
  <a href="#home"><img id="logo" src="images/2.jpg"  alt="Логотип"/></a>

  <h1>pixel.ru</h1>
</div>
</header>

<div class="container">
  <div class="forma">
    <?php
      
     
      if(isset($_COOKIE['loginAdmin'])){
        echo '<a href="antiLogin.php">Выйти из режима Администратора</a> <br> <a href="admin.php">Выбрать другого пользователя</a>';
        
        
      }
        

    ?>
  <h2 id="form">form.</h2>
  <form action="" method="POST">
    <ol>
      <?php
        if (!empty($messages)) {
        print('<div id="messages">');
        foreach ($messages as $message) {
          print($message);
        }
        print('</div>');
        }
      ?>
      <li>ФИО: <input name="fio" placeholder="Введите ФИО" 
      <?php if ($errors['fio']) {print 'class="fioerror"';} ?>
       value="<?php print $values['fio']; ?>" /> 
      </li>
      <li>email: <input name="email" placeholder="Введите email"
      <?php if ($errors['email']) {print 'class="emailerror"';} ?> value="<?php print $values['email']; ?>" />
      </li>
      <li>Дата рождения: <input name="yob" type="date"
      <?php if ($errors['yob']) {print 'class="yoberror"';} ?> value="<?php print $values['yob']; ?>" />
      </li>
      <li>
        Пол :
        <input type="radio" checked="checked" name="gender" value="man" <?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Мужчина
        <input type="radio" name="gender" value="woman" <?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Женщина
        <input type="radio" name="gender" value="hellicopter" <?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Сложно
        <input type="radio" name="gender" value="another" <?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Другое
       
      </li>
      <li>
        Количество конечностей:
        <input type="radio" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} if($values['n_limbs']=="1"){print 'checked';}?> value="1"/>1
        <input type="radio" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} if($values['n_limbs']=="2"){print 'checked';}?> value="2"/>2
        <input type="radio" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} if($values['n_limbs']=="3"){print 'checked';}?> value="3"/>3
        <input type="radio"  name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} if($values['n_limbs']=="4"){print 'checked';}?> value="4"/>4
        <input type="radio" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} if($values['n_limbs']=="5"){print 'checked';}?> value="5"/>5
        
      </li>
      <li>
        Сверхспособности :
        <select name="sp-sp"  <?php if ($errors['sp-sp']) {print 'class="sp-sperror"';} ?>>
          <option value="immortality" <?php if($values['sp-sp']=="immortality"){print 'selected';} ?> >Бессмертие</option>
          <option value="passing_through_walls" <?php if($values['sp-sp']=="passing_through_walls"){print 'selected';} ?> >Прохождение сквозь стены</option>
          <option value="levitation" <?php if($values['sp-sp']=="levitation"){print 'selected';} ?> >Левитация</option>
          <option value="wonder" <?php if($values['sp-sp']=="wonder"){print 'selected';} ?> >Вечное счастье</option>
        </select>
        
      </li>
      <li>
        Биография :<textarea name="bio"  
        <?php if ($errors['bio']) {print 'class="bioerror"';} ?> 
        placeholder="Введите свою биографию" ><?php print $values['bio']; ?></textarea>
        
      </li>
      <li>
        <input type="checkbox" name="Galochka" 
        <?php if ($errors['Galochka']) {print 'class="Galochkaerror"';} ?> />Принимаю условия
        
      </li>
      <li>
        <input type="submit" value="Отправить" />
      </li>
    </ol>
    <?php
      if(!isset($_COOKIE['login'])){
        ?>
        <a href="login.php">У меня уже есть пользователь</a>
        <?php
      }
      else{
        ?>
        <a href="antiLogin.php">Хочу создать нового пользователя</a>
        <?php
      }
    ?>
    
  </form>
</div>
</div>
<footer>
<h2>pixel.</h2>
</footer>
</body>
</html>